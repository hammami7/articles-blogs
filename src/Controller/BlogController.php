<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Comment;
use App\Form\ArticleType;
use App\Form\CommentType;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class BlogController extends AbstractController
{
    // Liste des articles sur la partie publique
    /**
     * @Route("/blog", name="blog")
     */
    public function index(ArticleRepository $repo, Request $request, PaginatorInterface $paginator)
    {

        //$donnees = $repo->findAll();
        $donnees = $repo->findArticles();

        $articles = $paginator->paginate(
            $donnees, // on passe les données
            $request->query->getInt('page', 1), // N° de la page en cours, 1 par défaut
            3 // nombre d'éléments par page
        );

        return $this->render('blog/index.html.twig', [
            'controller_name' => 'BlogController',
            'articles' => $articles,
        ]);
    }

    // Page d'accueil publique
    /**
     * @Route("/", name="home")
     */
    public function home(ArticleRepository $repo, Request $request, PaginatorInterface $paginator) {
        //$donnees = $repo->findAll();
        $donnees = $repo->findArticles();

        $articles = $paginator->paginate(
            $donnees, // on passe les données
            $request->query->getInt('page', 1), // N° de la page en cours, 1 par défaut
            3 // nombre d'éléments par page
        );
        return $this->render('blog/home.html.twig', [
            'title' => "Bienvenue sur la page du blogs 2021",
            'articles' => $articles,
        ]);
    }

    // la fonction qui reçoit l'ordre de creation d'article est placée AVANT la fonction d'affichage show()
    // ceci afin de prioriser la fonction create()
    // il peut en effet y avoir des "confusion de route" entre /blog/{id} et /blog/new
    // car Symfony peut penser que new (dans /blog/new) est en en fait un id (dans /blog/{id})

    // Ajout / Edition des articles dans la partie Admin

    /**
     * @Route("/admin/blog/create", name="create_article")
     */
    public function createAjaxAction(Article $article = null, Request $request, EntityManagerInterface $manager)
    {
        if ($request->isXMLHttpRequest()) {
            if ($request->getMethod() == 'POST') {
                $data = $request->request->all();
                $article = new Article();
                $article->setTitle($data['title']);
                $article->setReference($data['reference']);
                $article->setStatus($data['status']);
                $article->setPrix($data['prix']);
                $article->setAdresseFournisseur($data['adresse_fournisseur']);
                $article->setContent($data['content']);
                $article->setImages($data['img']);
                $article->setManuelUtilisation($data['manuel_utilisation']);
                $article->setCreatedAt(new \DateTime());

                $manager->persist($article);
                $manager->flush();
                return new JsonResponse(array('data' => 'aaaaaaaaaaaaaaa'));
            }
        }
        return new Response('This is not ajax!', 400);
    }

    /**
     * @Route("/admin/blog/edit", name="edite_article")
     */
    public function editAjaxAction(Article $article = null, Request $request, EntityManagerInterface $manager)
    {
        if ($request->isXMLHttpRequest()) {
            if ($request->getMethod() == 'POST') {
                $data = $request->request->all();
                $article = new Article();
                $article->setTitle($data['title']);
                $article->setReference($data['reference']);
                $article->setStatus($data['status']);
                $article->setPrix($data['prix']);
                $article->setAdresseFournisseur($data['adresse_fournisseur']);
                $article->setContent($data['content']);
                $article->setImages($data['img']);
                $article->setManuelUtilisation($data['manuel_utilisation']);
                $article->setCreatedAt(new \DateTime());
                $article->setUpdateAt(new \DateTime());

                $manager->persist($article);
                $manager->flush();
                return new JsonResponse(array('data' => 'aaaaaaaaaaaaaaa'));
            }
        }
        return new Response('This is not ajax!', 400);
    }
    /**
     * @Route("/admin/blog/new", name="blog_create")
     * @Route("/admin/blog/{id}/edit", name="blog_edit")
     */
    public function form(Article $article = null, EntityManagerInterface $manager) {

        if(!$article) {
            $article = new Article();
        } else {
            $article = $manager->getRepository('App\Entity\Article')->find($article->getId());
        }

        return $this->render('admin/blog/create.html.twig', [
                'editMode' => $article->getId() !== null,
                'article' => $article
            ]);

    }

    // Vue dans la partie publique qui permet d'afficher un article
    /**
     * @Route("/blog/{id}", name="blog_show")
     */
    public function show(Article $article, Request $request, EntityManagerInterface $manager) {

        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $comment->setCreatedAt(new \DateTime())
                    ->setArticle($article);
            $manager->persist($comment);
            $manager->flush();

            return $this->redirectToRoute('blog_show', [
                'id' => $article->getId()
            ]);
        }
    
        return $this->render('blog/show.html.twig', [
          'article' => $article,
          'commentForm' => $form->createView()
      ]);
    }

    // Suppression d'article
    /**
     * @Route("/admin/blog/{id}/delete", name="blog_delete", methods={"POST"})
     */
    public function delete(Request $request, Article $article): Response
    {
        if ($this->isCsrfTokenValid('delete'.$article->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();

            $destination = $this->getParameter('kernel.project_dir').'/public/uploads/article_image/';
            //unlink($destination.$article->getImages());

            $entityManager->remove($article);
            $entityManager->flush();
        }

        return $this->redirectToRoute('blog');
    }

}
